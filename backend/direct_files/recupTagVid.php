<?php
    require_once("../helper.php");
    
    $url = $_POST["urlVid"];

    $config = file_get_contents('../config.json');
    $config = json_decode($config);
    $res = array();
    foreach($config->services as $serv){
        if(strpos($url,$serv->adresse)!==false){
            $url_api = $serv->tags[0];
            $length = strlen($url) - strlen($serv->adresse);
            $search = substr($url,- $length);
            $url_api = str_replace("IDVID", $search, $url_api);
    
            //On récupère la clé si on en a besoin
            if($serv->pkey[0] == "yes"){
                $API_KEY = $serv->pkey[1];
                $url_api = str_replace("APIKEY", $API_KEY, $url_api);
            }

            try {
                $searchResponse = file_get_contents($url_api);
                $searchResponse = json_decode($searchResponse);
                $liste_final = array();
                foreach($serv->tags as $el){
                    if(is_numeric($el)){
                        $searchResponse = $searchResponse[$el];
                    }
                    else{
                        if($el != $serv->tags[0] && $el != "n"){
                            $searchResponse = $searchResponse->$el;
                        }
                        if($el == "n"){
                            break;
                        }
                    }
                }
                if($el == "n"){
                    foreach($searchResponse as $res){
                        array_push($liste_final,$res->tag);
                    }
                }
                else{
                    foreach($searchResponse as $res){
                        array_push($liste_final,$res);
                    }
                }
                sendMessage($liste_final);
            }
            catch (Exception $e) {
                continue;
            }
        }
    }


?>