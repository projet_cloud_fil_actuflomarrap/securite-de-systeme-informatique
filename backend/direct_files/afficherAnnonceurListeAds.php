<?php
    require_once("../mySQL/campagne.php");
    require_once("../mySQL/login.php");
    require_once("../mySQL/ads.php");
    require_once("../helper.php");

    $nomCampagne = $_POST["nomCampagne"];
    $nomAnnonceur = $_SESSION["login"];

    if(checkLogin($nomAnnonceur)){
        if($_SESSION["logged"]){
            if(verifType($nomAnnonceur)){
                try{
                    $idC = getIdC($nomCampagne,$nomAnnonceur);
                    $ids = getListeAds($idC);
                } catch(Exception $e){
                    sendError($e);
                }
                if(isset($ids[0])){
                    $liste_final = array();
                    foreach($ids as $ad){
                        $element = ["nomCampagne" => $nomCampagne, "nomAnnonceur" => $nomAnnonceur, "titre" => getInfoA($ad["id"])["titre"]];
                        array_push($liste_final,$element);
                    }
                    sendMessage($liste_final);
                }
                else{
                    sendError("Aucune annonce pour cette campagne");
                }
            }
            else{
                sendError("Utilisateur non annonceur");
            }
        }
        else{
            sendError("Utilisateur non connecté");
        }
    }
    else{
        sendError("Login non existant");
    }

?>