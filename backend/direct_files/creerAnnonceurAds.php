<?php
    require_once("../mySQL/campagne.php");
    require_once("../mySQL/login.php");
    require_once("../mySQL/ads.php");
    require_once("../helper.php");

    $titre = $_POST["titre"];
    $sous_titre = $_POST["sous_titre"];
    $type = $_POST["type"];
    $urlV = $_POST["url_video"];
    $urlA = $_POST["url_annonceur"];
    $format = $_POST["format"];
    $tags = explode(",", $_POST["tags"]);
    $active = 1;
    $clicks = 0;
    $cpc = $_POST["coutclick"];
    $debut = $_POST["debut"];
    $fin = $_POST["fin"];
    $nomCampagne = $_POST["nomCampagne"];
    $nomAnnonceur = $_SESSION["login"];

    if(checkLogin($nomAnnonceur)){
        if($_SESSION["logged"]){
            if(verifType($nomAnnonceur)){
                try{
                    $idV = getIdAds(getIdC($nomCampagne,$nomAnnonceur),$titre);
                } catch(Exception $e){
                }
                if(!isset($idV)){
                    $tag = "";
                    foreach($tags as $el){
                        $tag = $tag . $el . ",";
                    }
                    $tag = rtrim($tag, ",");
                    createAds($nomAnnonceur,getIdC($nomCampagne,$nomAnnonceur),$active,$titre,$sous_titre,$tag, $urlA, $urlV, $type ,$format,$debut,$fin, $clicks, $cpc);
                    sendMessage("OK");
                }
                else{
                    sendError("Publicité déjà existante !");
                }
            }
            else{
                sendError("Utilisateur non annonceur");
            }
        }
        else{
            sendError("Utilisateur non connecté");
        }
    }
    else{
        sendError("Login non existant");
    }
?>