<?php
    require_once("../mySQL/campagne.php");
    require_once("../mySQL/ads.php");
    require_once("../mySQL/login.php");
    require_once("../helper.php");

    $titre = $_POST["titre"];
    $sous_titre = $_POST["sous_titre"];
    $urlA = $_POST["url_annonceur"];
    $tags = $_POST["tags"];
    $début = $_POST["début"];
    $fin = $_POST["fin"];
    $activé = $_POST["activé"];
    $nomAnnonceur = $_SESSION["login"];
    $nomCampagne = $_POST["nomCampagne"];

    if(checkLogin($nomAnnonceur)){
        if($_SESSION["logged"]){
            if(verifType($nomAnnonceur)){
                try{
                    $idC = getIdC($nomCampagne,$nomAnnonceur);
                    $idAd = getIdAds($idC,$titre);
                } catch(Exception $e){
                    sendError($e);
                }
                if(isset($idAd)){
                    $info = getInfoA($idAd);
                    $tag = "";
                    foreach($tags as $el){
                        $tag = $tag . $el . ",";
                    }
                    $tag = rtrim($tag, ",");
                    modifV($idAd, $activé, $titre, $sous_titre, $tag, $urlA, $début, $fin);
                    sendMessage("OK");
                }
                else{
                    sendError("Aucune annonce de ce type");
                }
            }
            else{
                sendError("Utilisateur non annonceur");
            }
        }
        else{
            sendError("Utilisateur non connecté");
        }
    }
    else{
        sendError("Login non existant");
    }
?>