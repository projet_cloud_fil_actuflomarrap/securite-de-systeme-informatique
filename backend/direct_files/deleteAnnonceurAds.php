<?php
    require_once("../mySQL/campagne.php");
    require_once("../mySQL/login.php");
    require_once("../mySQL/ads.php");
    require_once("../helper.php");

    $nomAnnonceur = $_SESSION["login"];
    $nomCampagne = $_POST["nomCampagne"];
    $titre = $_POST["titre"];

    if(checkLogin($nomAnnonceur)){
        if($_SESSION["logged"]){
            if(verifType($nomAnnonceur)){
                try{
                    $idC = getIdc($nomCampagne,$nomAnnonceur);
                    $idAds = getIdAds($idC,$titre);
                } catch(Exception $e){
                    sendError($e);
                }
            
                if(isset($idAds)){
                    deleteAds($idAds);
                    sendMessage("OK");
                }
                else{
                    sendError("Annonce non existante");
                }
            }
            else{
                sendError("Utilisateur non annonceur");
            }
        }
        else{
            sendError("Utilisateur non connecté");
        }
    }
    else{
        sendError("Login non existant");
    }

?>