<?php
    require_once("../helper.php");
    require_once("../mySQL/video.php");

    $login = $_SESSION['login'];
    $idp = $_POST["idp"];
    $url = $_POST['url'];
    if($_SESSION['logged']==true){
    	if(!existsInPlaylist($idp, $url))
        {
        createVideo($idp,$url,$login);
        sendMessage("Vidéo ajoutée");
        }
    	else {
        	sendError("Vidéo existe déjà !");
        }
    }
    else{
        sendError("Non connecté");
    }
?>