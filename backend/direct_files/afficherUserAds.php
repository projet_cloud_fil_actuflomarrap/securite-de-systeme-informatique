<?php
    require_once("../mySQL/campagne.php");
    require_once("../mySQL/login.php");
    require_once("../mySQL/ads.php");
    require_once("../helper.php");

    $login = $_SESSION["login"];
    $tags = explode(",", $_POST["tags"]);

    if(checkLogin($login)){
        if($_SESSION["logged"]){
            if(isset($tags[0])){
                $liste_ads = array();
                $all_ads = getAll();
                foreach($all_ads as $ads){
                    foreach($tags as $tag){
                        $tagAds = getInfoA($ads["id"])["tags"];
                        if(stripos($tagAds,$tag) !== false){
                            if(!in_array($ads["id"],$liste_ads) && (getInfoA($ads["id"])["active"] == 1)){
                                array_push($liste_ads,$ads["id"]);
                            }
                        }
                    }
                }
                // Cas où il n'y aucune pub correspond aux tags
                if(count($liste_ads) == 0){
                	foreach($all_ads as $ads)
                    {
                    	array_push($liste_ads,$ads["id"]);
                    }
                }
                $click = -1;
                foreach($liste_ads as $ads){
                    if($click == -1){
                        $click = getInfoA($ads)["clicks"]; 
                    }
                    if($click >= getInfoA($ads)["clicks"]){
                        $click = getInfoA($ads)["clicks"];
                        $id = $ads;
                    }
                }
                $info = getInfoA($id);
                $ads_final = ["idA" => $info["idA"], "titre" => $info["titre"], "sous_titre" => $info["sous_titre"], "type" => $info["type"], "urlV" => $info["urlV"], "urlA" => $info["urlA"], "format" => $info["format"]];
            	sendMessage($ads_final);
            }
            else{
                $tags = [];
                sendError("Tags non rempli");
            }
        }
        else{
            sendError("Utilisateur non connecté");
        }
    }
    else{
        sendError("Login non existant");
    }

?>