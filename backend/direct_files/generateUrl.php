<?php
    require_once("../helper.php");
    $origin = $_POST["origin"];
    $video_code = $_POST["id"];
    
    $config = file_get_contents('../config.json');
    $config = json_decode($config);
    $res = array();
    foreach($config->services as $serv){
        if($origin == $serv->origin){
            $res = ["url" => $serv->adresse . $video_code];
        }
    }
    
    if(isset($res)){
        sendMessage($res);
    }
    else{
        sendError("Erreur lors de la génération de l'url");
    }
?>