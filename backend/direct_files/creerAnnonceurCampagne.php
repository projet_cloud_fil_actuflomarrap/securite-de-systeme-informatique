<?php
    require_once("../mySQL/campagne.php");
    require_once("../mySQL/login.php");
    require_once("../mySQL/ads.php");
    require_once("../helper.php");

    $activé = $_POST["activé"];
    $nomCampagne = $_POST["nomCampagne"];
    $capitalAlloué = $_POST["capitalAlloué"];
    $nomAnnonceur = $_SESSION["login"];
    
    if(checkLogin($nomAnnonceur)){
        if($_SESSION["logged"]){
            if(verifType($_POST["login"])){
                try{
                    $idC = getIdC($nomCampagne, $nomAnnonceur);
                } catch(Exception $e){
                    sendError($e);
                }
                if(!isset($idC)){
                    createCampagne($nomAnnonceur,$activé, $nomCampagne, $capitalAlloué);
                }
                else{
                    sendError("Nom de campagne déjà pris");
                }
                sendMessage("OK");
            }
            else{
                sendError("Utilisateur non annonceur");
            }
        }
        else{
            sendError("Utilisateur non connecté");
        }
    }
    else{
        sendError("Login non existant");
    }
?>