<?php
    require_once("../mySQL/campagne.php");
    require_once("../mySQL/ads.php");
    require_once("../mySQL/login.php");
    require_once("../helper.php");

    $nomCampagne = $_POST["nomCampagne"];
    $nomAnnonceur = $_SESSION["login"];
    $titre = $_POST["titre"];

    if(checkLogin($nomAnnonceur)){
        if($_SESSION["logged"]){
            if(verifType($nomAnnonceur)){
                try{
                    $idC = getIdC($nomCampagne,$nomAnnonceur);
                    $idAd = getIdAds($idC,$titre);
                } catch(Exception $e){
                }
                if(isset($idAd)){
                    $liste_final = array();
                    $info = getInfoA($idAd);
            
                    array_push($liste_final,$info["active"]);
                    array_push($liste_final,$info["debut"]);
                    array_push($liste_final,$info["fin"]);
                    array_push($liste_final,$info["titre"]);
                    array_push($liste_final,$info["type"]);
                    array_push($liste_final,$info["clicks"]);
                    array_push($liste_final,$info["coutClick"]);
                    sendMessage($info);
                }
                else{
                    sendError("Aucune annonce de ce type");
                }
            }
            else{
                sendError("Utilisateur non annonceur");
            }
        }
        else{
            sendError("Utilisateur non connecté");
        }
    }
    else{
        sendError("Login non existant");
    }



?>