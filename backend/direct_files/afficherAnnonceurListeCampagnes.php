<?php
    require_once("../mySQL/campagne.php");
    require_once("../mySQL/login.php");
    require_once("../mySQL/ads.php");
    require_once("../helper.php");

    $nomAnnonceur = $_SESSION["login"];
    
    if(checkLogin($nomAnnonceur)){
        if($_SESSION["logged"]){
            if(verifType($nomAnnonceur)){
                try{
                    $ids = getListeC($nomAnnonceur);
                } catch(Exception $e){
                    sendError($e);
                }
                if(isset($ids[0])){
                    $liste_final = array();
                    foreach($ids as $id){
                        $element = array();
                        $info = getInfoC($id["id"]);
                        array_push($element,$info["activé"]);
                        $début = date_create("3000-01-01");
                        $booli = false;
                        foreach(getListeAds($id["id"]) as $ad){
                            $date = date_create(getInfoA($ad["id"])["début"]);
                            if($date <= $début){
                                $début = $date->format("Y-m-d");
                                $booli = true; 
                            }
                        }
                        if(!$booli){
                            $début = "";
                        }
                        array_push($element,$début);
                        $fin = date_create("1900-01-01");
                        $booli = false;
                        foreach(getListeAds($id["id"]) as $ad){
                            $date = date_create(getInfoA($ad["id"])["début"]);
                            if($date >= $fin){
                                $fin = $date->format("Y-m-d");
                                $booli = true; 
                            }
                        }
                        if(!$booli){
                            $fin = "";
                        }
                        array_push($element,$fin);
                        array_push($element,$info["nom"]);
                        array_push($element,$info["capital"]);
                        $clicks = 0;
                        $capitalDépensé = 0;
                        foreach(getListeAds($id["id"]) as $ad){
                            $info = getInfoA($ad["id"]);
                            $cap = $info["coutClick"];
                            $c = $info["clicks"];
                            $clicks += $c;
                            $capitalDépensé += ($c * $cap);
                        }
                        array_push($element,$clicks);
                        array_push($element,$capitalDépensé);
                        array_push($liste_final,$element);
                    }
                    sendMessage($liste_final);
                }
                else{
                    sendError("Aucune campagne pour cet annonceur");
                }
            }
            else{
                sendError("Utilisateur non annonceur");
            }
        }
        else{
            sendError("Utilisateur non connecté");
        }
    }
    else{
        sendError("Login non existant");
    }
?>