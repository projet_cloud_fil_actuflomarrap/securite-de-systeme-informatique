<?php 
    require_once("../mySQL/campagne.php");
    require_once("../mySQL/ads.php");
    require_once("../mySQL/login.php");
    require_once("../helper.php");

    $nomAnnonceur = $_POST["nomAnnonceur"];
    $nomCampagne = $_POST["nomCampagne"];
    $login = $_SESSION["login"];
    $titre = $_POST["titre"];

    if(checkLogin($login)){
        if($_SESSION["logged"]){
            try{
                $idC = getIdC($nomCampagne, $nomAnnonceur);
                $idAds = getIdAds($idC,$titre);
            } catch(Exception $e){
                sendError($e);
            }
            if(isset($idAds)){
                modifClickAds($idAds,getInfoA($idAds)["clicks"]+1);
                sendMessage("OK");
            }
            else{
                sendError("Annonce non existante");
            }
        }
        else{
            sendError("Utilisateur non connecté");
        }
    }
    else{
        sendError("Login non existant");
    }

?>