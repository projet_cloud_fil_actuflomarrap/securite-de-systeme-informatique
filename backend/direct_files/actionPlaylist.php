<?php
    require_once("../helper.php");
    require_once("../mySQL/video.php");

    $action = $_POST["action"];
    if($_SESSION['logged'] == true){
        if($action == "del"){
            $idp = $_POST["idp"];
            $url = $_POST["url"];
            $login = $_SESSION["login"];
            deleteVideo($idp,$url,$login);
            sendMessage("Vidéo supprimée");
        }
        if($action == "up"){
            $idp = $_POST["idp"];
            $url = $_POST["url"];
            up($url, $idp);
            sendMessage("Vidéo remontée");
        }
        if($action == "down"){
            $idp = $_POST["idp"];
            $url = $_POST["url"];
            down($url, $idp);
            sendMessage("Vidéo descendue");
        }
    }
    else{
        sendError("Erreur ! Non connecté");
    }
?>