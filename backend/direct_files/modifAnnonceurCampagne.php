<?php
    require_once("../mySQL/campagne.php");
    require_once("../mySQL/login.php");
    require_once("../mySQL/ads.php");
    require_once("../helper.php");

    $nomAnnonceur = $_SESSION["login"];
    $nomCampagne = $_POST["nomCampagne"];
    $nouveauNom = $_POST["nouveauNom"];
    $capital = $_POST["capital"];
    $activé = $_POST["activé"];

    if(checkLogin($nomAnnonceur)){
        if($_SESSION["logged"]){
            if(verifType($nomAnnonceur)){
                try{
                    $idC = getIdc($nomCampagne,$nomAnnonceur);
                } catch(Exception $e){
                    sendError($e);
                }
                if(isset($idC)){
                    modifC($idC,$activé,$nouveauNom,$capital);
                    sendMessage("OK");
                }
                else{
                    sendError("Campagne non existante");
                }
            }
            else{
                sendError("Utilisateur non annonceur");
            }
        }
        else{
            sendError("Utilisateur non connecté");
        }
    }
    else{
        sendError("Login non existant");
    }

?>