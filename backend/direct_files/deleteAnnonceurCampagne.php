<?php
    require_once("../mySQL/campagne.php");
    require_once("../mySQL/login.php");
    require_once("../mySQL/ads.php");
    require_once("../helper.php");
    
    $nomAnnonceur = $_SESSION["login"];
    $nomCampagne = $_POST["nomCampagne"];

    if(checkLogin($nomAnnonceur)){
        if($_SESSION["logged"]){
            if(verifType($nomAnnonceur)){
                try{
                    $idC = getIdc($nomCampagne,$nomAnnonceur);
                    $liste_ads = getListeAds($idC);
                } catch(Exception $e){
                    sendError($e);
                }
                if(isset($idC)){
                    if(isset($liste_ads[0])){
                        foreach($liste_ads as $id){
                            deleteAds($id["id"]);
                        }
                    }
                    deleteCampagne($idC);
                    sendMessage("OK");
                }
                else{
                    sendError("Campagne non existante");
                }
            }
            else{
                sendError("Utilisateur non annonceur");
            }
        }
        else{
            sendError("Utilisateur non connecté");
        }
    }
    else{
        sendError("Login non existant");
    }

?>