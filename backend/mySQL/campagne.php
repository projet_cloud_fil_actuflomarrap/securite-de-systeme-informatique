<?php
    require_once("dbconnect.php");

	error_reporting(E_ALL);
	ini_set("display_errors", 1);


    function createCampagne($idA, $active, $nom, $capital){
        global $PDO; 
        $table = "Campagne";
        $query = "INSERT INTO $table (idA, active, nom, capital) VALUES (?,?,?,?)";
        $data = array($idA, $active, $nom, $capital);
        $statement = $GLOBALS["PDO"]->prepare($query);
        $exec = $statement->execute($data);
    }

    function deleteCampagne($id){
        global $PDO;
        $table = "Campagne";
        $query = "DELETE FROM $table WHERE id=?";
        $data = array($id);
        $statement = $GLOBALS["PDO"]->prepare($query);
        $exec = $statement->execute($data);
    }

    function getInfoC($id){
        global $PDO;
        $table = "Campagne";
        $query = "SELECT * FROM $table WHERE id=?";
        $data = array($id);
        $statement = $GLOBALS["PDO"]->prepare($query);
        $exec = $statement->execute($data);
		$resultats = $statement->fetchAll( PDO::FETCH_ASSOC );
        return($resultats[0]);
    }

    function getIdC($nom, $idA){
    	return $idA;
    	/*
        global $PDO;
        $table = "Campagne";
        $query = "SELECT id FROM $table WHERE nom=? AND idA=?";
        $data = array($nom, $idA);
        $statement = $GLOBALS["PDO"]->prepare($query);
        $exec = $statement->execute($data);
		$resultats = $statement->fetchAll( PDO::FETCH_ASSOC );
        return($resultats[0]["id"]);*/
    }

    function modifC($id, $active, $nom, $capital){
        global $PDO;
        $table = "Campagne";
        $query = "UPDATE $table SET active = ?, nom = ?, capital = ?  WHERE id = ?";
        $data = array($active, $nom, $capital, $id);
        $statement = $GLOBALS["PDO"]->prepare($query);
        $exec = $statement->execute($data);
		$resultats = $statement->fetchAll( PDO::FETCH_ASSOC );
    }

    function modifCapital($id, $valeur){
        global $PDO;
        $table = "Campagne";
        $query = "UPDATE $table SET capital=? WHERE id = ?";
        $data = array($valeur, $id);
        $statement = $GLOBALS["PDO"]->prepare($query);
        $exec = $statement->execute($data);
		$resultats = $statement->fetchAll( PDO::FETCH_ASSOC );
    }

    function getListeC($idA){
        global $PDO;
        $table = "Campagne";
        $query = "SELECT id FROM $table WHERE idA=?";
        $data = array($idA);
        $statement = $GLOBALS["PDO"]->prepare($query);
        $exec = $statement->execute($data);
		$resultats = $statement->fetchAll( PDO::FETCH_ASSOC );
        return($resultats);
    }
?>