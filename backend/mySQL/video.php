<?php
    require_once("dbconnect.php");

	error_reporting(E_ALL);
	ini_set("display_errors", 1);


    function getNom($idp){
        global $PDO;
        $table = "Video";
        $query = "SELECT nom, idu FROM $table WHERE idp=? AND url=?";
        $data = array($idp,"0");
        $statement = $GLOBALS["PDO"]->prepare($query);
        $exec = $statement->execute($data);
		$resultats = $statement->fetchAll( PDO::FETCH_ASSOC );
        return($resultats);
    }

    function createPlaylist($login,$nom){
        global $PDO; 
        $table = "Video";
        $query = "INSERT INTO $table (idu,nom,url,suivant,idp) VALUES (?,?,'0','',?)";
        $data = array($login,$nom, time());
        $statement = $GLOBALS["PDO"]->prepare($query);
        $exec = $statement->execute($data);
    }

    function renamePlaylist($idp, $nouveau_nom){
        global $PDO;
        $table = "Video";
        $query = "UPDATE $table SET nom = ? WHERE idp = ?";
        $data = array($nouveau_nom,$idp);
        $statement = $GLOBALS["PDO"]->prepare($query);
        $exec = $statement->execute($data);
    }

    function deletePlaylist($idPlaylist){
        global $PDO;
        $table = "Video";
        $query = "DELETE FROM $table WHERE idp=?";
        $data = array($idPlaylist);
        $statement = $GLOBALS["PDO"]->prepare($query);
        $exec = $statement->execute($data);
    }

    function getPlaylist($idp){
        global $PDO;
        $table = "Video";
        $query = "SELECT url, suivant FROM $table WHERE idp=?";
        $data = array($idp);
        $statement = $GLOBALS["PDO"]->prepare($query);
        $exec = $statement->execute($data);
		$resultats = $statement->fetchAll( PDO::FETCH_ASSOC );
        return($resultats);
    }

    function getPlaylists($login){
        global $PDO;
        $table = "Video";
        $query = "SELECT idp, nom FROM $table WHERE idu=? and url='0'";
        $data = array($login);
        $statement = $GLOBALS["PDO"]->prepare($query);
        $exec = $statement->execute($data);
		$resultats = $statement->fetchAll( PDO::FETCH_ASSOC );
        return($resultats);
    }

    //Regarde si une vidéo est dans une playlist
    function existsInPlaylist($idp,$url){
        global $PDO;
        $table = "Video";
        $query = "SELECT url FROM $table WHERE idp=? AND url=?";
        $data = array($idp,$url);
        $statement = $GLOBALS["PDO"]->prepare($query);
        $exec = $statement->execute($data);
		$resultats = $statement->fetchAll( PDO::FETCH_ASSOC );

        return count($resultats) != 0;
    }

    //Sert à changer le suivant du maillon
    function changeSuivant($idp,$url,$suivant){
        global $PDO;
        $table = "Video";
        $query = "UPDATE $table SET suivant = ? WHERE idp = ? AND url = ?";
        $data = array($suivant,$idp,$url);
        $statement = $GLOBALS["PDO"]->prepare($query);
        $exec = $statement->execute($data);
		$resultats = $statement->fetchAll( PDO::FETCH_ASSOC );
    }

    //Sert à chercher le maillon dont le suivant est l'url que l'on cherche
    function searchBefore($idp,$suivant){
        global $PDO;
        $table = "Video";
        $query = "SELECT url FROM $table WHERE suivant=? AND idp=?";
        $data = array($suivant, $idp);
        $statement = $GLOBALS["PDO"]->prepare($query);
        $exec = $statement->execute($data);
		$resultats = $statement->fetchAll( PDO::FETCH_ASSOC );

    	if(count($resultats) != 0)
        {
        	return($resultats[0]["url"]);
        }
    	else
        	return NULL;
    }

    function searchSuivant($idp,$url){
        global $PDO;
        $table = "Video";
        $query = "SELECT suivant FROM $table WHERE url=? AND idp=?";
        $data = array($url, $idp);
        $statement = $GLOBALS["PDO"]->prepare($query);
        $exec = $statement->execute($data);
		$resultats = $statement->fetchAll( PDO::FETCH_ASSOC );
    	if(count($resultats) != 0)
        	return($resultats[0]["suivant"]);
    	else
        	return NULL;
    }

    function compterVidéo($idp){
        global $PDO; 
        $table = "Video";
        $query = "SELECT COUNT(url) AS compte FROM $table WHERE idp=?";
        $data = array($idp);
        $statement = $GLOBALS["PDO"]->prepare($query);
        $exec = $statement->execute($data);
		$resultats = $statement->fetchAll( PDO::FETCH_ASSOC );
    	//print_r($resultats);
        return $resultats[0]["compte"];
    }

    //Sert à ajouter une vidéo à une playlist
    function createVideo($idp,$url,$login){
        global $PDO; 
        if(compterVidéo($idp) != 0) {
            changeSuivant($idp,searchBefore($idp,""),$url);
        }
        $table = "Video";
        $query = "INSERT INTO $table (idp,url,idu, suivant) VALUES (?,?,?,?)";
        $data = array($idp,$url,$login, "");
        $statement = $GLOBALS["PDO"]->prepare($query);
        $exec = $statement->execute($data);
    }

    //Supprime la vidéo de la playlist
    function deleteVideo($idp,$url,$login){
        global $PDO;
        if(compterVidéo($idp) != 0){
            if(searchSuivant($idp,$url) != NULL){
                changeSuivant($idp,searchBefore($idp,$url),searchSuivant($idp,$url));
            }
            else{
                changeSuivant($idp,searchBefore($idp,$url),"");
            }
            $table = "Video";
            $query = "DELETE FROM $table WHERE url=? AND idp=?";
            $data = array($url,$idp);
            $statement = $GLOBALS["PDO"]->prepare($query);
            $exec = $statement->execute($data);
        }
    }

    //Reste à faire la partie up et down
    function up($url,$idp){
        $compte = compterVidéo($idp);
        if($compte > 1){
            $av = searchBefore($idp,$url);
            if($av == NULL){
            	echo $url;
            }
            else{
                $av_av = searchBefore($idp,$av);
                //cas où il y en a 2 avant
                if($av_av != NULL){
                    changeSuivant($idp,$av_av,$url);
                }
                $af = searchSuivant($idp,$url);
                if($af != NULL){
                    changeSuivant($idp,$av,$af);
                }
                else{
                    changeSuivant($idp,$av,"");
                }
                changeSuivant($idp,$url,$av);
            	
            }
        }
    }
    
    function down($url,$idp){
        $compte = compterVidéo($idp);
        if($compte > 1){
            $af = searchSuivant($idp,$url);
            if($af == NULL){}
            else{
                $af_af = searchSuivant($idp,$af);
            	$av = searchBefore($idp,$url);
                //cas où il y en a 2 avant
                if($af_af != NULL){
                    changeSuivant($idp,$url,$af_af);
                }
                else{
                    changeSuivant($idp,$url,"");
                }
                
                if($av != NULL){
                    changeSuivant($idp,$av,$af);
                }
                changeSuivant($idp,$af,$url);
            }
        }
    }
    
?>