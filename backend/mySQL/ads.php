<?php
    require_once("dbconnect.php");

    error_reporting(E_ALL);
    ini_set("display_errors", 1);


    function createAds($idA, $idC, $active, $titre, $sous_titre, $tags, $urlA, $urlV, $type, $format, $debut, $fin, $clicks, $coutClick){
        global $PDO; 
        $table = "Ads";
        $query = "INSERT INTO $table (idA, idC, active, titre, sous_titre, tags, urlA, urlV ,type, format, debut, fin, clicks, coutClick) VALUES (?,?,?,?,?,?,?,?,?,?,?,?,?,?)";
        $data = array($idA, $idC, $active, $titre, $sous_titre, $tags, $urlA, $urlV, $type, $format, $debut, $fin, $clicks, $coutClick);
        $statement = $GLOBALS["PDO"]->prepare($query);
        $exec = $statement->execute($data);
    }

    function deleteAds($id){
        global $PDO;
        $table = "Ads";
        $query = "DELETE FROM $table WHERE id=?";
        $data = array($id);
        $statement = $GLOBALS["PDO"]->prepare($query);
        $exec = $statement->execute($data);
    }

    function getInfoA($id){
        global $PDO;
        $table = "Ads";
        $query = "SELECT * FROM $table WHERE id=?";
        $data = array($id);
        $statement = $GLOBALS["PDO"]->prepare($query);
        $exec = $statement->execute($data);
		$resultats = $statement->fetchAll( PDO::FETCH_ASSOC );
        return($resultats[0]);
    }

    function getIdAds($idC, $titre){
        global $PDO;
        $table = "Ads";
        $query = "SELECT id FROM $table WHERE idC=? AND titre=?";
        $data = array($idC, $titre);
        $statement = $GLOBALS["PDO"]->prepare($query);
        $exec = $statement->execute($data);
		$resultats = $statement->fetchAll( PDO::FETCH_ASSOC );
    	if(count($resultats) != 0)
        	return($resultats[0]["id"]);
    	else
        	return NULL;
    }

    function modifV($id, $activé, $titre, $sous_titre, $tags, $urlA, $debut, $fin){
        global $PDO;
        $table = "Ads";
        $query = "UPDATE $table SET activé = ?, titre = ?, sous_titre = ?, urlA = ?, tags = ?, debut = ?, fin = ? WHERE id = ?";
        $data = array($activé, $titre, $sous_titre, $urlA, $tags, $debut, $fin, $id);
        $statement = $GLOBALS["PDO"]->prepare($query);
        $exec = $statement->execute($data);
		$resultats = $statement->fetchAll( PDO::FETCH_ASSOC );
    }

    function getListeAds($idC){
        global $PDO;
        $table = "Ads";
        $query = "SELECT id FROM $table WHERE idC=?";
        $data = array($idC);
        $statement = $GLOBALS["PDO"]->prepare($query);
        $exec = $statement->execute($data);
		$resultats = $statement->fetchAll( PDO::FETCH_ASSOC );
        return($resultats);
    }

    function getAll(){
        global $PDO;
        $table = "Ads";
        $query = "SELECT id FROM $table";
        $data = array();
        $statement = $GLOBALS["PDO"]->prepare($query);
        $exec = $statement->execute($data);
		$resultats = $statement->fetchAll( PDO::FETCH_ASSOC );
        return($resultats);
    }

    function modifClickAds($id, $clicks){
        global $PDO;
        $table = "Ads";
        $query = "UPDATE $table SET clicks = ? WHERE id = ?";
        $data = array($clicks,$id);
        $statement = $GLOBALS["PDO"]->prepare($query);
        $exec = $statement->execute($data);
		$resultats = $statement->fetchAll( PDO::FETCH_ASSOC );
    }
?>