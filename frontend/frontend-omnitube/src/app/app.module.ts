import { NgModule } from '@angular/core';
import { BrowserModule } from '@angular/platform-browser';
import {MatInputModule} from '@angular/material/input';
import { AppRoutingModule } from './app-routing.module';
import { AppComponent } from './app.component';
import { LoginComponent } from './login/login.component';
import { BannerComponent } from './banner/banner.component';
import { FormsModule } from '@angular/forms';
import { HttpClientModule } from '@angular/common/http';
import { MainPageComponent } from './main-page/main-page.component';
import { AdsComponent } from './ads/ads.component';
import { CreateAdsComponent } from './create-ads/create-ads.component';
import { WatchComponent } from './watch/watch.component';
import {MatBottomSheetModule} from '@angular/material/bottom-sheet';
import { BrowserAnimationsModule } from '@angular/platform-browser/animations';
import { MatListModule } from '@angular/material/list'; 
import {MatSelectModule} from '@angular/material/select';
import { PlaylistComponent } from './playlist/playlist.component';
import {TextFieldModule} from '@angular/cdk/text-field';
import { ChangerMdpComponent } from './changer-mdp/changer-mdp.component'; 
import {MatTableModule} from '@angular/material/table';
import {MatDatepickerModule} from '@angular/material/datepicker';
import { MatNativeDateModule } from '@angular/material/core';
import { MatMomentDateModule } from '@angular/material-moment-adapter';
import {MatCheckboxModule} from '@angular/material/checkbox';
import { AngularFileUploaderModule } from "angular-file-uploader";
import { PubliciteComponent } from './publicite/publicite.component';
import { ModifyAdsComponent } from './modify-ads/modify-ads.component';


@NgModule({
  declarations: [
    AppComponent,
    LoginComponent,
    BannerComponent,
    MainPageComponent,
    AdsComponent,
    CreateAdsComponent,
    WatchComponent,
    PlaylistComponent,
    ChangerMdpComponent,
    PubliciteComponent,
    ModifyAdsComponent
  ],
  imports: [
    BrowserModule,
    FormsModule,
    AppRoutingModule,
    HttpClientModule,
    MatBottomSheetModule,
    BrowserAnimationsModule,
    MatListModule,
    MatSelectModule,
    TextFieldModule,
    MatTableModule,
    MatInputModule,
    MatDatepickerModule,
    MatNativeDateModule,
    MatMomentDateModule,
    MatCheckboxModule,
    AngularFileUploaderModule

  ],
  providers: [],
  bootstrap: [AppComponent]
})
export class AppModule { }
