import { HttpClient, HttpHandler } from '@angular/common/http';
import { Component, OnInit } from '@angular/core';
import { MessageService } from '../message/message.service';
import { Router } from '@angular/router';
import { AuthService } from '../auth.service'
import { PhpData } from '../message/message.service'
import { BannerComponent } from '../banner/banner.component';

@Component({
  selector: 'app-login',
  templateUrl: './login.component.html',
  styleUrls: ['./login.component.scss']
})
export class LoginComponent implements OnInit {
  error_msg = '';
  error_msg2 = '';
  pseudo = '';
  mdp = '';
  pseudo2 = '';
  mdp2 = '';
  mdp2_conf = '';
  email = '';
  type = 0;
  constructor(private ms : MessageService, private as : AuthService, private router: Router) {}

  ngOnInit(): void {
    BannerComponent.fonction_appelée();
  }
  
  getError(num : number) : string {
    if(num == 1)
	    return this.error_msg;
    else
      return this.error_msg2;
  }
  
  setError(num : number, msg : string): void {
    if(num == 1)
	    this.error_msg = msg;
    else
      this.error_msg2 = msg;  
  }

  check() : void {
    this.as.sendAuthentication(this.pseudo, this.mdp).subscribe((msg: any) => {
      if(this.as.finalizeAuthentication(msg))
        this.router.navigateByUrl('/main-page');
      else
          this.setError(1, msg.data.reason);
    });
  }

  createAccount() : void {
    if(this.mdp2 == this.mdp2_conf) {
      this.ms.sendMessage("createAccount", {login: this.pseudo2, mdp: this.mdp2, mail: this.email, type: this.type}).subscribe((msg: any) => {
        if(msg.status == "ok")
          this.router.navigateByUrl('/main-page');
        else
            this.setError(2, msg.data.reason);
      });
    }
    else
        this.setError(2, "Le mot de passe donné n'est pas le bon.");
  }
}
