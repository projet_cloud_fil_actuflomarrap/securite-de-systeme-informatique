import { Injectable } from '@angular/core';
import { HttpClient } from '@angular/common/http';
import { Observable } from 'rxjs';
import { environment } from 'src/environments/environment';

export interface PhpData {
	status : string, data : any

};

@Injectable({
  providedIn: 'root'
})
export class MessageService {

  constructor(private http : HttpClient) { }
  getMessage() : Observable<PhpData> {
    return this.http.get<PhpData>(
      'http://127.0.0.1/forum/helper.php'
    );
  }
  sendMessage(url : string, data : Object, file : File | null = null) : Observable<PhpData> {
    var complete_url = environment.prefix + url + '.php';
    var form_data = new FormData();
    if(data != null && data != undefined)
    {
      for (const [key, value] of Object.entries(data)) {
        form_data.append(key, value);
      }
    }
    if(file != null)
      form_data.append('file', file, file.name);
    return this.http.post<PhpData>(complete_url, form_data, {withCredentials: true});
  }
}
