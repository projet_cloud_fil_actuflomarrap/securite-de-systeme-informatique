import { Component } from '@angular/core';
import { MatTableDataSource } from '@angular/material/table';
import { Router } from '@angular/router';
import { BannerComponent } from '../banner/banner.component';
import { DataService } from '../data.service';
import { MessageService } from '../message/message.service';

@Component({
  selector: 'app-publicite',
  templateUrl: './publicite.component.html',
  styleUrls: ['./publicite.component.scss']
})
export class PubliciteComponent {
  constructor(private router: Router, private ms : MessageService, private ds : DataService) { }
  nom = "";
  id = -1;
  ngOnInit(): void {
    BannerComponent.fonction_appelée();
    if(this.ds.data.nom != undefined)
    {
      this.id = this.ds.data.id;
      this.nom = this.ds.data.nom;
    }
    else {
      this.id = 0;
      this.nom = "";;
      //this.router.navigateByUrl("/ads");
    }
    this.ds.data = {};
    this.afficher();
  }
  displayedColumns = ["active", "periode", "annonce", "type", "clics", "coutparclic", "totaldepense"];

  dataSource = new MatTableDataSource([{id:0, active: true, periode: { start: new Date(), end: new Date() }, annonce:"", type:"", clics:78, coutparclic:12, totaldepense:56, nomCampagne:""}]);

  selectedIndex! : number | null;

  setDisplayed(row:Object) :void {
    for(var i = 0; i <  document.getElementsByTagName("table")[0].rows.length; i++) {
      let obj = document.getElementsByTagName("table")[0].rows.item(i + 1);
      if(obj != null)
        obj.style.backgroundColor = (this.dataSource.data.at(i) == row ? "lightgrey" : "white");
        if(this.dataSource.data.at(i) == row)
        this.selectedIndex = i;
    }
  }

  afficher() {
    let liste : Array<any> = [];
    this.dataSource.data = liste;
    let nomCampagne = "La camargue";
    this.ms.sendMessage("afficherAnnonceurListeAds", {nomCampagne: nomCampagne}).subscribe((msg: any) => {
      if(msg.status == "ok")
      {
        for(let ad of msg.data)
        {
          this.ms.sendMessage("afficherAnnonceurAds", {nomCampagne: ad.nomCampagne, titre:ad.titre}).subscribe((msg2: any) => {
            if(msg2.status == "ok") {
              let infos = {id:0, active: msg2.data.active, periode: { start: msg2.data.debut, end: msg2.data.fin }, annonce:msg2.data.titre, type:msg2.data.type, clics:msg2.data.clicks, coutparclic:msg2.data.coutClick, totaldepense:msg2.data.clicks*msg2.data.coutClick, nomCampagne:""};
              console.log(infos);
              this.dataSource.data.push(infos);
              this.dataSource.data = this.dataSource.data;
            }
          });
        }
        
      }
    });
    
  }

  creer() : void {
    this.router.navigateByUrl('/create-ads');
  }

  supprimer() : void {
    if(this.selectedIndex!=null) {
      this.ms.sendMessage("supprimerPublicite", {id: this.dataSource.data.at(this.selectedIndex)}).subscribe((msg: any) => {});
      this.dataSource.data.splice(this.selectedIndex,1);
      this.selectedIndex = null;
      this.dataSource.data = this.dataSource.data;
    }
  }

  double_clic() : void {
    if(this.selectedIndex != null)
    {
      let index = this.dataSource.data.at(this.selectedIndex);
      if(index != undefined)
      {
        this.ds.data.id_pub = index.id;
        this.ds.data.nom_pub = index.annonce;
        this.router.navigateByUrl("modify-ads");
      }
    }
  }
}
