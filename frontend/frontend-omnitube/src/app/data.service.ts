import { Injectable } from '@angular/core';

@Injectable({
  providedIn: 'root'
})
export class DataService {

   _data: any;

   set data(data: any) {
      this._data = data;
   }

   get data(): any {
       return this._data;
   }

   constructor() {
    this._data = {};
   }

}
