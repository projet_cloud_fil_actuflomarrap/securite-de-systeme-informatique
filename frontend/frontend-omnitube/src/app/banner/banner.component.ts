import { Component, OnInit } from '@angular/core';
import { Router } from '@angular/router';
import { AuthService, ProfileType } from '../auth.service';

@Component({
  selector: 'app-banner',
  templateUrl: './banner.component.html',
  styleUrls: ['./banner.component.scss']
})
export class BannerComponent implements OnInit {

  searchReq = '';
  constructor(private router: Router, private auth: AuthService) { }
  ngOnInit(): void {
    BannerComponent.fonction_appelée();
  }
  static fonction_appelée(): void {
    let bloc = document.getElementById("bloc_recherche");
    let id_bouton = document.getElementById("login_button");
    let pub_bouton = document.getElementById("pub_button");
    let subtitle = document.getElementById("subtitle");
    if(bloc != null && id_bouton != null && pub_bouton != null && subtitle != null) {
      if(!AuthService.connected) {
        bloc.style.display = "none";
        id_bouton.style.display = "none";

      }
      else {
        bloc.style.display = "block";
        id_bouton.style.display = "block";
      }

      if(AuthService.profileType == ProfileType.SPECTATEUR)
      {
        subtitle.style.display = "none";
        pub_bouton.style.display = "none";  
      }
      else {
        subtitle.style.display = "block";  
        pub_bouton.style.display = "block";
      }
    }
  }

  onClic(b:boolean): void {
    this.router.routeReuseStrategy.shouldReuseRoute = () => false;
    this.router.onSameUrlNavigation = 'reload';
    if(/([\s\w]*)/.exec(this.searchReq) != null)
    {
      if(b)
      {
        this.router.navigateByUrl('/main-page?search=' + this.searchReq.replace(/\s/, "+"));
      }
      else
        this.router.navigateByUrl('/main-page');
    }
  }

  dconnecter() : void {
    if(AuthService.connected)
    {
      this.auth.endAuthentication().subscribe(msg=>{
        if(msg.status=="ok")
          this.router.navigateByUrl('/login');
      });
    }
  }

}
