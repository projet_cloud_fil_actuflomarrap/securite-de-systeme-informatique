import { Component } from '@angular/core';
import { Router } from '@angular/router';
import { AuthService } from '../auth.service';
import { BannerComponent } from '../banner/banner.component';
import { MessageService } from '../message/message.service';

@Component({
  selector: 'app-changer-mdp',
  templateUrl: './changer-mdp.component.html',
  styleUrls: ['./changer-mdp.component.scss']
})
export class ChangerMdpComponent {
  email = '';
  mdp2 = '';
  mdp2_conf = '';
  error_msg = '';

  constructor(private ms : MessageService, private as : AuthService, private router: Router) {}

  ngOnInit(): void {
    BannerComponent.fonction_appelée();
  }

  setError(msg : string): void {
	    this.error_msg = msg;
  }

  changerMdp() : void {
    if(this.mdp2 == this.mdp2_conf) {
      this.ms.sendMessage("changerMdp", {email: this.email, mdp2: this.mdp2}).subscribe((msg: any) => {
        if(msg.status == "ok")
            this.router.navigateByUrl('/login');
        else
            this.setError(msg.data.reason);
      });
    }
    else {
      this.setError("Les mots de passe ne correspondent pas.");
    }
  }
}
