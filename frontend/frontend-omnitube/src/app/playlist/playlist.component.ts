import { Component } from '@angular/core';
import {ActivatedRoute} from '@angular/router';
import { Router } from '@angular/router';
import { BannerComponent } from '../banner/banner.component';
import { MessageService } from '../message/message.service';

@Component({
  selector: 'app-playlist',
  templateUrl: './playlist.component.html',
  styleUrls: ['./playlist.component.scss']
})
export class PlaylistComponent {

  date:string="none";
  type!:string;
  vues:string="none";

  playlist_id!:string;
  nom : string = "";
  auteur : string = "";

  liste_video : Array<any> = [{
    code:'hy3W-3HPMWg',
    nom:'Terry Riley - A Rainbow in Curved Air - Full CD (HQ)',
    origine:'yt',
    origine_long:'Youtube',
    score:'1.1M vues',
    miniature:"https://i3.ytimg.com/vi/hy3W-3HPMWg/maxresdefault.jpg"
  }, 
  {
    code:'hy3W-3HPMWg',
    nom:'Terry Riley - A Rainbow in Curved Air - Full CD (HQ)',
    origine:'yt',
    origine_long:'Youtube',
    score:'1.1M vues',
    miniature:"https://i3.ytimg.com/vi/hy3W-3HPMWg/maxresdefault.jpg"
  }, 
  {
    code:'hy3W-3HPMWg',
    nom:'Terry Riley - A Rainbow in Curved Air - Full CD (HQ)',
    origine:'yt',
    origine_long:'Youtube',
    score:'1.1M vues',
    miniature:"https://i3.ytimg.com/vi/hy3W-3HPMWg/maxresdefault.jpg"
  },
  {
    code:'hy3W-3HPMWg',
    nom:'Terry Riley - A Rainbow in Curved Air - Full CD (HQ)',
    origine:'yt',
    origine_long:'Youtube',
    score:'1.1M vues',
    miniature:"https://i3.ytimg.com/vi/hy3W-3HPMWg/maxresdefault.jpg"
  },
  {
    code:'hy3W-3HPMWg',
    nom:'Terry Riley - A Rainbow in Curved Air - Full CD (HQ)',
    origine:'yt',
    origine_long:'Youtube',
    score:'1.1M vues',
    miniature:"https://i3.ytimg.com/vi/hy3W-3HPMWg/maxresdefault.jpg"
  },
  {
    code:'hy3W-3HPMWg',
    nom:'Terry Riley - A Rainbow in Curved Air - Full CD (HQ)',
    origine:'yt',
    origine_long:'Youtube',
    score:'1.1M vues',
    miniature:"https://i3.ytimg.com/vi/hy3W-3HPMWg/maxresdefault.jpg"
  },
  {
    code:'hy3W-3HPMWg',
    nom:'Terry Riley - A Rainbow in Curved Air - Full CD (HQ)',
    origine:'yt',
    origine_long:'Youtube',
    score:'1.1M vues',
    miniature:"https://i3.ytimg.com/vi/hy3W-3HPMWg/maxresdefault.jpg"
  },
  {
    code:'hy3W-3HPMWg',
    nom:'Terry Riley - A Rainbow in Curved Air - Full CD (HQ)',
    origine:'yt',
    origine_long:'Youtube',
    score:'1.1M vues',
    miniature:"https://i3.ytimg.com/vi/hy3W-3HPMWg/maxresdefault.jpg"
  },
  {
    code:'hy3W-3HPMWg',
    nom:'Terry Riley - A Rainbow in Curved Air - Full CD (HQ)',
    origine:'yt',
    origine_long:'Youtube',
    score:'1.1M vues',
    miniature:"https://i3.ytimg.com/vi/hy3W-3HPMWg/maxresdefault.jpg"
  }]

  constructor(private route: ActivatedRoute, private router: Router, private ms: MessageService) {

  }

  ngOnInit() {
    BannerComponent.fonction_appelée();
    this.route.queryParams
    .subscribe((params: any) => {
      this.playlist_id = params['id'];
    });
    this.loadResults();
  }

  loadResults() {
    this.ms.sendMessage("getPlaylist", {idp: this.playlist_id}).subscribe((msg: any) => {
      if(msg.status == "ok")
      {
        this.nom = msg.data.nom;
        this.auteur = msg.data.auteur;
        /*this.liste_video.push({
          code:'hy3W-3HPMWg',
          nom:'Terry Riley - A Rainbow in Curved Air - Full CD (HQ)',
          origine:'yt',
          origine_long:'Youtube',
          score:'1.1M vues',
          miniature:"https://i3.ytimg.com/vi/hy3W-3HPMWg/maxresdefault.jpg"
        });*/
        //this.liste_video = msg.data.videos;
        this.liste_video = msg.data.videos;

        console.log(msg.data);
        console.log(this.liste_video);
      }
    });
  }

  voirVideo(video_id : string, video_service : string): void{
    this.router.navigateByUrl('/watch?video_id=' + video_id + '&video_service=' + video_service);
  }

  renommerPlaylist(): void{
    this.ms.sendMessage("renamePlaylist", {idp: this.playlist_id, nom: this.nom}).subscribe((msg: any) => {});
  }

  actionPlaylist(video_id : string, origin : string, action : string): void{
    this.ms.sendMessage("generateUrl", {id: video_id, origin : origin}).subscribe((msg: any) => {
      if(msg.status == "ok")
        if(!(msg.data.length == 1 && action == "del"))
        this.ms.sendMessage("actionPlaylist", {idp: this.playlist_id, url: msg.data.url, action: action}).subscribe((msg: any) => {
          if(msg.status == "ok")
          {
            this.loadResults();
          }
        });
    });
  }

  deletePlaylist(): void{
    this.ms.sendMessage("deletePlaylist", {idp: this.playlist_id}).subscribe((msg: any) => {
      if(msg.status == "ok")
      {
        this.router.navigateByUrl('/main-page');
      }
    });
  }
}
