import { Component, Injectable, OnInit, ViewChild } from '@angular/core';
import { MatTable, MatTableDataSource } from '@angular/material/table';
import { Router } from '@angular/router';
import { MessageService } from '../message/message.service';
import { DataService } from '../data.service';


@Component({
  selector: 'app-ads',
  templateUrl: './ads.component.html',
  styleUrls: ['./ads.component.scss']
})
export class AdsComponent implements OnInit {

  constructor(private router: Router, private ms : MessageService, private ds : DataService) { }

  displayedColumns = ["active", "periode", "groupe", "clictot", "capalloc", "capdep"];

  dataSource = new MatTableDataSource([{id: 0, active: true, periode: { start: new Date(), end: new Date() }, groupe:"Abc", clictot:78, capalloc:12, capdep:56},
    {id: 1, active: true, periode: { start: new Date(), end: new Date() }, groupe:"Def", clictot:78, capalloc:12, capdep:56}])

  ngOnInit(): void {
    this.afficherCampagnes();
  }

  selectedIndex! : number | null;


  afficherCampagnes() {
    this.ms.sendMessage("afficherAnnonceurListeCampagnes", {}).subscribe((msg: any) => {
      if(msg.status == "ok")
      {
        this.dataSource.data = msg.data;
      }
    });
  }

  setDisplayed(row:Object) :void {
    for(var i = 0; i <  document.getElementsByTagName("table")[0].rows.length; i++) {
      let obj = document.getElementsByTagName("table")[0].rows.item(i + 1);
      if(obj != null)
        obj.style.backgroundColor = (this.dataSource.data.at(i) == row ? "lightgrey" : "white");
        if(this.dataSource.data.at(i) == row)
        this.selectedIndex = i;
    }
  }

  creer() : void {
    //this.dataSource.data.push({id: this.dataSource.data.length, active: false, periode: { start: new Date(), end: new Date() }, groupe:"", clictot:0, capalloc:0, capdep:0});
    this.dataSource.data = this.dataSource.data;
    //this.ms.sendMessage("creerAnnonceurCampagne", {id: this.dataSource.data.at(this.selectedIndex)}).subscribe((msg: any) => {});
  }

  supprimer() : void {
    if(this.selectedIndex!=null) {
      this.ms.sendMessage("supprimerGroupePublicite", {id: this.dataSource.data.at(this.selectedIndex)}).subscribe((msg: any) => {});
      this.dataSource.data.splice(this.selectedIndex,1);
      this.selectedIndex = null;
      this.dataSource.data = this.dataSource.data;
    }
  }

  double_clic() : void {
    if(this.selectedIndex != null)
    {
      let index = this.dataSource.data.at(this.selectedIndex);
      if(index != undefined)
      {
        this.ds.data.id = index.id;
        this.ds.data.nom = index.groupe;
        this.router.navigateByUrl("ad");
      }
    }
  }

}
