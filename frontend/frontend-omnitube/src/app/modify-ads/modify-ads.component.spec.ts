import { ComponentFixture, TestBed } from '@angular/core/testing';

import { ModifyAdsComponent } from './modify-ads.component';

describe('ModifyAdsComponent', () => {
  let component: ModifyAdsComponent;
  let fixture: ComponentFixture<ModifyAdsComponent>;

  beforeEach(async () => {
    await TestBed.configureTestingModule({
      declarations: [ ModifyAdsComponent ]
    })
    .compileComponents();

    fixture = TestBed.createComponent(ModifyAdsComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
