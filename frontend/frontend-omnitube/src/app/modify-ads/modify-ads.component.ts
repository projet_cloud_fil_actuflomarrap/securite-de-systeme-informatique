import { Component } from '@angular/core';
import { Router } from '@angular/router';
import { BannerComponent } from '../banner/banner.component';
import { DataService } from '../data.service';
import { MessageService } from '../message/message.service';

@Component({
  selector: 'app-modify-ads',
  templateUrl: './modify-ads.component.html',
  styleUrls: ['./modify-ads.component.scss']
})
export class ModifyAdsComponent {

  constructor(private ms : MessageService, private router: Router, private ds : DataService) { }

  ngOnInit(): void {
    BannerComponent.fonction_appelée();
    this.load();
  }
  titre! : string;
  sous_titre! : string;
  URL! : string;
  tags! : string;
  type! : string;
  periode! : any;

  resetVar! : any;
  afuConfig : any = {
    multiple: false,
    formatsAllowed: ".jpg,.png,.mp4",
    maxSize: "1",
    uploadAPI:  {
      url:"https://example-file-upload-api",
      method:"POST",
      headers: {
     "Content-Type" : "text/plain;charset=UTF-8",
      },
      params: {
        'page': '1'
      },
      responseType: 'blob',
      withCredentials: false,
    },
    theme: "dragNDrop",
    hideProgressBar: false,
    hideResetBtn: false,
    hideSelectBtn: false,
    fileNameIndex: true,
    autoUpload: false,
    replaceTexts: {
      selectFileBtn: 'Sélectionner un fichier',
      resetBtn: 'Réinitialiser',
      uploadBtn: 'Mettre en ligne',
      dragNDropBox: 'Glisser-déposer...',
      attachPinBtn: 'Attach Files...',
      afterUploadMsg_success: 'Successfully Uploaded !',
      afterUploadMsg_error: 'Upload Failed !',
      sizeLimit: 'Size Limit'
    }
};

fileToUpload: File | null = null;

docUpload(e:any) : void {

}

fileSelected(e:any) : void {
  console.log(e.target.files);
  this.fileToUpload = e.target.files.item(0);
}

load() : void {
  this.ms.sendMessage("afficherAnnonceurAds", {nomCampagne: this.ds.data.nom, titre: this.ds.data.nom_pub}).subscribe((msg: any) => {
    if(msg.status == "ok") {
      this.titre = msg.data.titre;
      this.sous_titre = msg.data.sous_titre;
      this.URL = msg.data.URL;
      this.tags = msg.data.tags.toString();
      this.type = msg.data.type;
    }
  });
}

  valider() : void {
      if(this.fileToUpload != null)
      {
        let format = this.fileToUpload.name.split(".").slice(-1);
        let coutclick = this.fileToUpload.size;
        this.ms.sendMessage("modifAnnonceurAds", {titre: this.titre, sous_titre: this.sous_titre, type: this.type, url_annonceur: this.URL, format: format, tag: this.tags, coutclick: coutclick, nomCampagne: this.ds.data.nom}).subscribe((msg: any) => {
          if(msg.status == "ok") {
            this.router.navigateByUrl('/ads');
          }
        });
      } 
  }
}
