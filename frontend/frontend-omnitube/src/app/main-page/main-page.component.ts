import { Component } from '@angular/core';
import {ActivatedRoute} from '@angular/router';
import { Router } from '@angular/router';
import { BannerComponent } from '../banner/banner.component';
import { MessageService } from '../message/message.service';

@Component({
  selector: 'app-main-page',
  templateUrl: './main-page.component.html',
  styleUrls: ['./main-page.component.scss']
})
export class MainPageComponent {

  date:string="none";
  type:string[] = ["yt", "dm", "vm"];
  vues:string="none";

  search!:string;

  liste_video = [{
    code:'',
    nom_video:'',
    origine:'',
    origine_long:'',
    score:'',
    vignette:""
  }]

  constructor(private route: ActivatedRoute, private router: Router, private ms: MessageService) {

  }

  ngOnInit() :void {
    BannerComponent.fonction_appelée();
    this.route.queryParams
    .subscribe((params: any) => {
      this.search = params['search'];
    });
    this.loadResults();
  }

  loadResults(): void {
    console.log(this.search);
    if(this.search == undefined)
      this.search = '';
    this.ms.sendMessage("get_videos", {search:this.search, date:this.date, type:this.type, vues:this.vues, order:"date"}).subscribe((msg: any) => {
      console.log(msg.data);
      if(msg.status == "ok") {
        this.liste_video = msg.data;
      }
    });
  }

  voirVideo(video_id : string, video_service : string): void{
    this.router.navigateByUrl('/watch?video_id=' + video_id + '&video_service=' + video_service);
  }
}
