import { Component, OnInit } from '@angular/core';
import { Router } from '@angular/router';
import { AngularFileUploaderComponent } from 'angular-file-uploader';
import { BannerComponent } from '../banner/banner.component';
import { DataService } from '../data.service';
import { MessageService } from '../message/message.service';

@Component({
  selector: 'app-create-ads',
  templateUrl: './create-ads.component.html',
  styleUrls: ['./create-ads.component.scss']
})
export class CreateAdsComponent implements OnInit {

  constructor(private ms : MessageService, private router: Router, private ds : DataService) { }

  ngOnInit(): void {
    BannerComponent.fonction_appelée();
  }
  titre! : string;
  sous_titre! : string;
  URL! : string;
  tags! : string;
  type! : string;
  url_video! : string;
  start = new Date();
  end = new Date();

  resetVar! : any;
  afuConfig : any = {
    multiple: false,
    formatsAllowed: ".jpg,.png,.mp4",
    maxSize: "1",
    uploadAPI:  {
      url:"https://example-file-upload-api",
      method:"POST",
      headers: {
     "Content-Type" : "text/plain;charset=UTF-8",
      },
      params: {
        'page': '1'
      },
      responseType: 'blob',
      withCredentials: false,
    },
    theme: "dragNDrop",
    hideProgressBar: false,
    hideResetBtn: false,
    hideSelectBtn: false,
    fileNameIndex: true,
    autoUpload: false,
    replaceTexts: {
      selectFileBtn: 'Sélectionner un fichier',
      resetBtn: 'Réinitialiser',
      uploadBtn: 'Mettre en ligne',
      dragNDropBox: 'Glisser-déposer...',
      attachPinBtn: 'Attach Files...',
      afterUploadMsg_success: '',
      afterUploadMsg_error: '',
      sizeLimit: 'Size Limit'
    }
};

fileToUpload: File | null = null;

docUpload(e:any) : void {
  this.ms.sendMessage("loadFile", {}, this.fileToUpload).subscribe((msg: any) => {
    if(msg.status == "ok") {
      this.url_video = msg.data.target;
    }
  });
}

fileSelected(e:any) : void {
  console.log(e.target.files);
  this.fileToUpload = e.target.files.item(0);
}

date_format(date_elm : Date) {
  let date_str = date_elm.toLocaleDateString("fr-FR").replace(/\//g, "-").split("-");

  return date_str[2] + "-" + date_str[1] + "-" + date_str[0];
}

valider() : void {
  if(this.fileToUpload != null)
  {
  let format = this.fileToUpload.name.split(".").slice(-1);
  let coutclick = this.fileToUpload.size;
  this.ms.sendMessage("creerAnnonceurAds", {active: 1, titre: this.titre, sous_titre: this.sous_titre, type: this.type, url_video: this.url_video, url_annonceur: this.URL, format: format, tags: this.tags.split(","), coutclick: coutclick, nomCampagne: "La camargue", 
    debut: this.date_format(this.start), fin: this.date_format(this.end)}).subscribe((msg: any) => {
    if(msg.status == "ok") {
      this.router.navigateByUrl('/publicite');
    }
  });
} 
}
}
