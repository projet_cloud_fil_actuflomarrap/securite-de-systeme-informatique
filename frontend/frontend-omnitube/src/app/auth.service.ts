import { Injectable } from '@angular/core';
import { PhpData, MessageService } from './message/message.service';
import { Observable } from 'rxjs';
import { NONE_TYPE } from '@angular/compiler';

export enum ProfileType {
  SPECTATEUR = 0,
  ANNONCEUR = 1
};

@Injectable({
  providedIn: 'root'
})
export class AuthService {

  public static connected = false;
  public static profileType : ProfileType = ProfileType.SPECTATEUR;

  constructor(private ms : MessageService) { }

  sendAuthentication(login: string, password : string) : Observable<PhpData> {
    return this.ms.sendMessage('checkLogin', {login: login, mdp: password});
  }

  endAuthentication() : Observable<PhpData> {
    return this.ms.sendMessage('disconnect', {});
  }

  finalizeAuthentication(msg : PhpData) : boolean {
    AuthService.connected = msg.status=="ok";
    console.log(msg.data);
    AuthService.profileType = msg.data.profileType;
    return AuthService.connected;
  }
}

