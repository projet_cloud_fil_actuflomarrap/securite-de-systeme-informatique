import { Component, TemplateRef, ViewChild, OnInit, Input, AfterViewInit } from '@angular/core';
import { ActivatedRoute, Router } from '@angular/router';
import { DomSanitizer } from '@angular/platform-browser';
import {MatBottomSheet} from '@angular/material/bottom-sheet';
import { MessageService } from '../message/message.service';
import { BannerComponent } from '../banner/banner.component';

@Component({
  selector: 'app-watch',
  templateUrl: './watch.component.html',
  styleUrls: ['./watch.component.scss']
})
export class WatchComponent implements OnInit, AfterViewInit {

  url = '';
  url_pub = '';

  nom!: string;
  date!: string;
  score!: number;

  source = '';

  video_id = '';
  video_service = '';

  add_playlist_name = '';

  url_site! : string;

  playlists : Array<any> = [];

  recommandations = [{
    code:'hy3W-3HPMWg',
    nom_video:'Terry Riley - A Rainbow in Curved Air - Full CD (HQ)',
    origine:'yt',
    origine_long:'Youtube',
    score:'1.1M vues',
    vignette:"https://i3.ytimg.com/vi/hy3W-3HPMWg/maxresdefault.jpg"
  }, 
  {
    code:'hy3W-3HPMWg',
    nom_video:'Terry Riley - A Rainbow in Curved Air - Full CD (HQ)',
    origine:'yt',
    origine_long:'Youtube',
    score:'1.1M vues',
    vignette:"https://i3.ytimg.com/vi/hy3W-3HPMWg/maxresdefault.jpg"
  }, 
  {
    code:'hy3W-3HPMWg',
    nom_video:'Terry Riley - A Rainbow in Curved Air - Full CD (HQ)',
    origine:'yt',
    origine_long:'Youtube',
    score:'1.1M vues',
    vignette:"https://i3.ytimg.com/vi/hy3W-3HPMWg/maxresdefault.jpg"
  },
  {
    code:'hy3W-3HPMWg',
    nom_video:'Terry Riley - A Rainbow in Curved Air - Full CD (HQ)',
    origine:'yt',
    origine_long:'Youtube',
    score:'1.1M vues',
    vignette:"https://i3.ytimg.com/vi/hy3W-3HPMWg/maxresdefault.jpg"
  },
  {
    code:'hy3W-3HPMWg',
    nom_video:'Terry Riley - A Rainbow in Curved Air - Full CD (HQ)',
    origine:'yt',
    origine_long:'Youtube',
    score:'1.1M vues',
    vignette:"https://i3.ytimg.com/vi/hy3W-3HPMWg/maxresdefault.jpg"
  },
  {
    code:'hy3W-3HPMWg',
    nom_video:'Terry Riley - A Rainbow in Curved Air - Full CD (HQ)',
    origine:'yt',
    origine_long:'Youtube',
    score:'1.1M vues',
    vignette:"https://i3.ytimg.com/vi/hy3W-3HPMWg/maxresdefault.jpg"
  },
  {
    code:'hy3W-3HPMWg',
    nom_video:'Terry Riley - A Rainbow in Curved Air - Full CD (HQ)',
    origine:'yt',
    origine_long:'Youtube',
    score:'1.1M vues',
    vignette:"https://i3.ytimg.com/vi/hy3W-3HPMWg/maxresdefault.jpg"
  },
  {
    code:'hy3W-3HPMWg',
    nom_video:'Terry Riley - A Rainbow in Curved Air - Full CD (HQ)',
    origine:'yt',
    origine_long:'Youtube',
    score:'1.1M vues',
    vignette:"https://i3.ytimg.com/vi/hy3W-3HPMWg/maxresdefault.jpg"
  },
  {
    code:'hy3W-3HPMWg',
    nom_video:'Terry Riley - A Rainbow in Curved Air - Full CD (HQ)',
    origine:'yt',
    origine_long:'Youtube',
    score:'1.1M vues',
    vignette:"https://i3.ytimg.com/vi/hy3W-3HPMWg/maxresdefault.jpg"
  }]

  @ViewChild('templateBottomSheet')
  TemplateBottomSheet!: TemplateRef<any>;

  constructor(private ms : MessageService, private route: ActivatedRoute, private router: Router, public sanitizer: DomSanitizer, private bottomSheet: MatBottomSheet) { }
  ngAfterViewInit(): void {

  }

  ngOnInit(): void {
    BannerComponent.fonction_appelée();
    this.route.queryParams
      .subscribe((params: any) => {
        this.video_id = params['video_id'];
        this.video_service = params['video_service'];
        let url = '';
        if(this.video_service == "dm"){
          url = "https://www.dailymotion.com/embed/video/" + this.video_id + "?autoplay=1";
          this.source = "DAILYMOTION";
        }
        else  if(this.video_service == "yt") {
          url = "https://www.youtube.com/embed/" + this.video_id + "?autoplay=1";
          this.source = "YOUTUBE";
        }
        else  if(this.video_service == "vm") {
          url = "https://player.vimeo.com/video/" + this.video_id  + "?autoplay=1";
          this.source = "VIMEO";
        }
        this.url = url;
      }
    );
    this.getInformation();
    this.loadRecommandations();
    this.loadPlaylists();
    this.loadPublicite();
    //this.balancerLaSauce.bind(this);
    //this.arreterLaSauce.bind(this);
    //setTimeout(this.balancerLaSauce, 15000);
  }

  loadPublicite() : void {
    this.ms.sendMessage("afficherUserAds", {tags: [this.nom.split(" ")]}).subscribe((msg: any) => {
      if(msg.status == "ok")
      {
        console.log(msg.data);
        this.url_pub = "https://martin-padovani-etu.pedaweb.univ-amu.fr/extranet/urbanisation/direct_files/" + msg.data.urlV;
        document.getElementById("img_pub")?.setAttribute("src", this.url_pub);
        this.url_site = msg.data.urlA;
      }
    });
  }

  balancerLaSauce() : void {
    let video = document.getElementById("video");
    let attrb = String(video?.getAttribute("src"));
    video?.setAttribute("src", attrb.slice(0,-11) + "?autoplay=0");
    let pub = document.getElementById("pub_avec_video");

    //this.loadPublicite();

    if(pub != null) {
      pub.style.display = "block";
    }
    console.log("test1");
    setTimeout(this.arreterLaSauce, 10000);
  }

  arreterLaSauce() : void {
    console.log("test2");
    let video = document.getElementById("video");
    let attrb = String(video?.getAttribute("src"));
    video?.setAttribute("src", attrb.slice(0,-("?autoplay=0&disablekb=1".length)) + "?autoplay=1");
    let pub = document.getElementById("pub_avec_video");
    if(pub != null)
      pub.style.display = "none";
    setTimeout(this.balancerLaSauce, 15000);
  }

  voirVideo(video_id : string, video_service : string): void{
    this.router.navigateByUrl('/watch?video_id=' + video_id + '&video_service=' + video_service);
  }

  loadPlaylistTotal() {
    for(let p of this.playlists)
    {
      this.ms.sendMessage("existsInPlaylist", {idp: p.idp, url: this.url.slice(0,-11)}).subscribe((msg: any) => {
        if(msg.status=="ok") {
          let box = document.getElementById(p.idp + "_checked");
          (box as HTMLInputElement).checked = msg.data;
          console.log(msg.data);
        }
      });
    }
  }

  openTemplateSheetMenu() {
    this.bottomSheet.open(this.TemplateBottomSheet);
    this.loadPlaylistTotal();
  }

  closeTemplateSheetMenu() {
    this.bottomSheet.dismiss();
  }

  addPlaylist() {
    this.ms.sendMessage("addPlaylist", {nomPlaylist: this.add_playlist_name}).subscribe((msg: any) => {});
    this.loadPlaylists();
    this.loadPlaylistTotal();
  }

  getInformation() {
    this.ms.sendMessage("getInformation", {idvid: this.video_id, originvid: this.video_service}).subscribe((msg: any) => {
      if(msg.status == "ok")
      {
        this.date = new Date(msg.data.date * 1000).toDateString();
        this.nom = msg.data.nom;
        this.score = msg.data.score;
        this.loadPublicite();
      }
    });
  }

  loadPlaylists() {
    this.ms.sendMessage("getPlaylists", {}).subscribe((msg: any) => {
      if(msg.status == "ok")
      {
        this.playlists = msg.data;
      }
    });

  }

  goTo(id: number) {
    this.closeTemplateSheetMenu();
    var router = this.router;
    setTimeout(function() {
      router.navigateByUrl('/playlist?id=' + id);
    }, 1000);
  }

  modifyVideo(idp: number) {
    let b = false;
    this.ms.sendMessage("existsInPlaylist", {idp: idp, url: this.url.slice(0,-11)}).subscribe((msg: any) => {
      if(msg.status=="ok") {
        b = msg.data;
      }
    });
    if(!b)
    {
      this.ms.sendMessage("addVideo", {idp: idp, url: this.url.slice(0,-11)}).subscribe((msg: any) => {});
      this.goTo(idp);
    }
    else
      this.ms.sendMessage("actionPlaylist", {idp: idp, url: this.url.slice(0,-11), action:"del"}).subscribe((msg: any) => {});
  }

  loadRecommandations() {
    this.ms.sendMessage("getRecommandation", {video_id: this.video_id}).subscribe((msg: any) => {
      if(msg.status == "ok")
      {
        this.recommandations = msg.data;
      }
    });
  }
  

  goToAd() {
    window.location.href = this.url_site;
  }
}