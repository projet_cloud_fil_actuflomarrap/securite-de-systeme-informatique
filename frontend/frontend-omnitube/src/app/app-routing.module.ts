import { NgModule } from '@angular/core';
import { RouterModule, Routes } from '@angular/router';
import { LoginComponent } from './login/login.component';
import { ChangerMdpComponent } from './changer-mdp/changer-mdp.component';
import { WatchComponent } from './watch/watch.component';
import { MainPageComponent } from './main-page/main-page.component';
import { PlaylistComponent } from './playlist/playlist.component';
import { AdsComponent } from './ads/ads.component';
import { PubliciteComponent } from './publicite/publicite.component';
import { CreateAdsComponent } from './create-ads/create-ads.component';
import { ModifyAdsComponent } from './modify-ads/modify-ads.component';
import { AuthGuard } from './auth.guard';

const routes: Routes = [
  {path: '', pathMatch: 'full', redirectTo: 'login'},
  {path: 'login', component: LoginComponent},
  {path: 'changer-mdp', component: ChangerMdpComponent},
  {path: 'watch', component: WatchComponent, canActivate: [AuthGuard]},
  {path: 'main-page', component: MainPageComponent, canActivate: [AuthGuard]},
  {path: 'playlist', component: PlaylistComponent,canActivate: [AuthGuard]},
  {path: 'ads', component: AdsComponent,canActivate: [AuthGuard]},
  {path: 'ad', component: PubliciteComponent,canActivate: [AuthGuard]},
  {path: 'create-ads', component: CreateAdsComponent ,canActivate: [AuthGuard]},
  {path: 'modify-ads', component: ModifyAdsComponent ,canActivate: [AuthGuard]}
];

@NgModule({
  imports: [RouterModule.forRoot(routes)],
  exports: [RouterModule]
})
export class AppRoutingModule { }
